#include "sicxeasm.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){

    if(argc == 1){
        printf("No input file.\n");
        return 0;
    }
    if(argc != 3){
        printf("Output file not specified.\n");
        return 0;
    }
    //open file
    FILE *in_fp = fopen(argv[1], "r");
    if(!ckptr(in_fp)){
        printf("File not found!\n");
        return -1;
    }
    //get size
    size_t fsize = 0;
    fseek(in_fp, 0, SEEK_END); //reach to the end
    fsize = ftell(in_fp);
    fseek(in_fp, 0, SEEK_SET); //reset ptr
    //validate fsize
    if(fsize <= 0){
        printf("Invalid fsize given.\n");
        return -1;
    }
    
    printf("File size: %d byte\n", fsize);

    FILE *out_fp = fopen(argv[2], "w");
    if(!ckptr(out_fp)){
        printf("Could not create file!\n");
        return -1;
    }
    compile(in_fp, out_fp, fsize);

    //free everything
    fclose(in_fp);
    fclose(out_fp);
    return 0;
}
