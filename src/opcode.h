#pragma once

#include <stdint.h>

enum OpType{
        Fmt1 = 0,
        Fmt2,
        Fmt34, // type 3/4
};

enum OperandType {
    NONE = 0,
    REG,
    ADDR,
    LITERAL
};

typedef struct {
    char *token; // key
    enum OpType fmt;
    uint8_t mcode;
    uint8_t operand_n;
    enum OperandType operand_a;
    enum OperandType operand_b;
} OPCode;

#define OPCODE_LEN 59
static const OPCode OPCODE[OPCODE_LEN] = {
    {"ADD", Fmt34, 0x18, 1, ADDR},
    {"ADDF", Fmt34, 0x58, 1, ADDR},
    {"ADDR", Fmt2, 0x90, 2, REG, REG},
    {"AND", Fmt34, 0x40, 1, ADDR},
    {"CLEAR", Fmt2, 0xb4, 1, REG},
    {"COMP", Fmt34, 0x28, 1, ADDR},
    {"COMPF", Fmt34, 0x88, 1, ADDR},
    {"COMPR", Fmt2, 0xa0, 2, REG, REG},
    {"DIV", Fmt34, 0x24, 1, ADDR},
    {"DIVF", Fmt34, 0x64, 1, ADDR},
    {"DIVR", Fmt2, 0x9c, 2, REG, REG},
    {"FIX", Fmt34, 0xc4, 0},
    {"FLOAT", Fmt34, 0xc0, 0},
    {"HIO", Fmt34, 0xf4, 0},
    {"J", Fmt34, 0x3c, 1, ADDR},
    {"JEQ", Fmt34, 0x30, 1, ADDR},
    {"JGT", Fmt34, 0x34, 1, ADDR},
    {"JLT", Fmt34, 0x38, 1, ADDR},
    {"JSUB", Fmt34, 0x48, 1, ADDR},
    {"LDA", Fmt34, 0x00, 1, ADDR},
    {"LDB", Fmt34, 0x68, 1, ADDR},
    {"LDCH", Fmt34, 0x50, 1, ADDR},
    {"LDF", Fmt34, 0x70, 1, ADDR},
    {"LDL", Fmt34, 0x08, 1, ADDR},
    {"LDS", Fmt34, 0x6c, 1, ADDR},
    {"LDT", Fmt34, 0x74, 1, ADDR},
    {"LDX", Fmt34, 0x04, 1, ADDR},
    {"LPS", Fmt34, 0xe0, 1, ADDR},
    {"UML", Fmt34, 0x20, 1, ADDR},
    {"MULF", Fmt34, 0x60, 1, ADDR},
    {"MULR", Fmt2, 0x98, 2, REG, REG},
    {"NORM", Fmt1, 0xc8, 0},
    {"OR", Fmt34, 0x44, 1, ADDR},
    {"RD", Fmt34, 0xd8, 1, ADDR},
    {"RMO", Fmt2, 0xac, 2, REG, REG},
    {"RSUB", Fmt34, 0x4c, 0},
    {"SHIFTL", Fmt2, 0xa4, 2, REG, LITERAL},
    {"SHIFTR", Fmt2, 0xa8, 2, REG, LITERAL},
    {"SIO", Fmt1, 0xf0, 0},
    {"SSK", Fmt34, 0xec, 1, ADDR},
    {"STA", Fmt34, 0x0c, 1, ADDR},
    {"STB", Fmt34, 0x78, 1, ADDR},
    {"STCH", Fmt34, 0x54, 1, ADDR},
    {"STF", Fmt34, 0x80, 1, ADDR},
    {"STI", Fmt34, 0xd4, 1, ADDR},
    {"STL", Fmt34, 0x14, 1, ADDR},
    {"STS", Fmt34, 0x7c, 1, ADDR},
    {"STSW", Fmt34, 0xe8, 1, ADDR},
    {"STT", Fmt34, 0x84, 1, ADDR},
    {"STX", Fmt34, 0x10, 1, ADDR},
    {"SUB", Fmt34, 0x1c, 1, ADDR},
    {"SUBF", Fmt34, 0x5c, 1, ADDR},
    {"SUBR", Fmt2, 0x94, 2, REG, REG},
    {"SVC", Fmt2, 0xb0, 1, LITERAL},
    {"TD", Fmt34, 0xe0, 1, ADDR},
    {"TIO", Fmt1, 0xf8, 0},
    {"TIX", Fmt34, 0x2c, 1, ADDR},
    {"TIXR", Fmt2, 0xb8, 1, REG},
    {"WD", Fmt34, 0xdc, 1, ADDR}
};

//指令格式
typedef uint8_t InstType1; //1 bytes
#pragma pack(1)
typedef struct
{
    uint8_t r2: 4;
    uint8_t r1: 4;
    uint8_t op;
} InstType2; //2 bytes

typedef struct
{
    uint16_t disp : 12;
    uint8_t e: 1; //0: Type3 1: Type4
    uint8_t p: 1;
    /*
        b|p|desc
        ----------------
        0|0|Directive
        1|0|Base related
        0|1|PC related
    */
    uint8_t b: 1;
    uint8_t x: 1; // Indexed-addressing mode
    
    /*
        i|n|desc
        ----------------
        0|0|Simple addressing
        0|1|Indirect addressing
        1|0|Immediate addressing
        1|1|Simple addressing
    */
    // uint8_t i: 1;
    // uint8_t n: 1;
    uint8_t op;
}InstType3; //3 bytes

typedef struct
{
    uint32_t address: 20;
    uint8_t e: 1;
    uint8_t p: 1;
    uint8_t b: 1;
    uint8_t x: 1;
    // uint8_t i: 1;
    // uint8_t n: 1;
    uint8_t op;
}InstType4; //4 bytes


