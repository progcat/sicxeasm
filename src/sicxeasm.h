#pragma once
#include <stdint.h>
#include <stdio.h>

#include "opcode.h"
#include "uthash/uthash.h"

#define DEBUG

#define ckptr(x) (x != NULL)
#define valid24bit(x) (!(x & 0xff000000))
#define isReg(x) (x == 'A' || x == 'X' || x == 'L' || x == 'B' || x == 'S' || x == 'T' || x == 'F')

//Directives
#define ASMDIR_START "START"
#define ASMDIR_END "END"
#define ASMDIR_BYTE "BYTE"
#define ASMDIR_BASE "BASE"
#define ASMDIR_EQU "EQU"
#define ASMDIR_WORD "WORD"
#define ASDIR_NOBASE "NOBASE"
#define ASMDIR_ORG "ORG"
#define ASMDIR_RESB "RESB"
#define ASMDIR_RESW "RESW"
#define ASMDIR_USE "USE"
#define ASMDIR_LTORG "LTORG"
#define ASMDIR_EXTDEF "EXTDEF"
#define ASMDIR_EXTREF "EXTREF"

#define SYMFLAG_LOCAL 0
#define SYMFLAG_EXTDEF 1
#define SYMFLAG_EXTREF 2
#define SYMFLAG_EQUDEF 4

//Registers
#define REG_A 0
#define REG_X 1
#define REG_L 2
#define REG_B 3
#define REG_S 4
#define REG_T 5
#define REG_F 6
// #define REG_PC 8
// #define REG_SW 9

typedef struct 
{
    char *ident; // key
    uint32_t line;
    uint16_t section;
    uint32_t address;
    /*
        bit     -   desc
        --------------------------------------
        1       -   0: Local Symbol, 1: Public Symbol(EXTDEF)
        2       -   0: Local Symbol, 1: External Symbol
        3       -   0: Symbol, 1: EQU Define
    */
    uint8_t flag;
    UT_hash_handle hh;
} Symbol;

typedef struct 
{
    uint32_t section_id; //key
    char *ident;
    uint32_t address;
    uint32_t length;
    UT_hash_handle hh;
} Block;

typedef struct 
{
    uint32_t data; // data as key
    uint32_t section_id;
    uint32_t line;
    uint32_t address;
    uint32_t length;
    uint8_t relocate;
    UT_hash_handle hh;
} Literal;

typedef struct
{
    char *prog_name;
    uint32_t start_addr;
    uint32_t prog_length;
} ProgramHdr;

typedef struct _instruction_node
{
    uint32_t address;
    uint8_t break_here;
    enum InstType {
        Inst_1 = 0,
        Inst_2,
        Inst_3,
        Inst_4
    } type;
    union
    {
        InstType1 *inst_1;
        InstType2 *inst_2;
        InstType3 *inst_3;
        InstType4 *inst_4;
    } inst_ptr;
    struct _instruction_node *next;
} InstructionNode;

// sicxeasm.c
char *nextToken();
int32_t compile(FILE *in_fp, FILE *out_fp, size_t fsize);
int32_t buildTables(FILE *fp, size_t fsize, uint32_t start_loc);
int32_t parseOpCode(FILE *fp, size_t fsize, uint32_t start_loc);
void trimStr(char *str);

// tables.c
extern Symbol *symtab_head;
extern Block *blktab_head;
extern Literal *literaltab_head;

void resolveBlkAddr();
void resolveAddresses();

int32_t appendSymbol(char *ident, uint32_t addr, uint32_t line, uint32_t sid, uint8_t flag);
Symbol *lookupSymbol(char *ident);
int32_t symbolExist(char *ident);
void printSymtab();

Block* appendBlock(char *ident, uint32_t addr, uint32_t length);
Block *lookupBlock(char *ident);
Block *getBlock(uint32_t sid);
int32_t blockExist(char *ident);
void printBlktab();

int32_t appendLiteral(uint32_t *data, uint32_t sid, uint32_t line, uint32_t address, uint32_t length, uint8_t need_relocate);
Literal *lookupLiteral(uint32_t data);
int32_t existLiteral(uint32_t data);
void printLiteraltab();

// directives.c
int32_t parseDirective(char *token, uint32_t *curr_addr, uint32_t *curr_section, size_t line);
uint32_t *parseLiteral(char *token, uint32_t *len_out);

// codegen.c
int32_t genObjcode(FILE *fp, InstructionNode *head, ProgramHdr *prog_hdr);
int32_t dump_inst(InstructionNode *head);
uint32_t swapInst(InstructionNode *node);
int32_t writeTextRecord(FILE *fp, InstructionNode *start, InstructionNode *end);