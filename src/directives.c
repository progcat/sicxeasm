#include "sicxeasm.h"
#include <ctype.h>
//TODO: handle EQU, BASE, NOBASE, END
int parseDirective(char *token, uint32_t *curr_addr, uint32_t *curr_section, size_t line) {
    if (!strcmp(token, ASMDIR_RESB)) {
        token = nextToken();
        int size = atoi(token);
        if (!size) {
            printf("[ERROR] atoi failed\n");
            return -1;
        }
        *curr_addr += size;
    } else if (!strcmp(token, ASMDIR_RESW)) {
        token = nextToken();
        int size = atoi(token);
        if (!size) {
            printf("[ERROR] atoi failed\n");
            return -1;
        }
        *curr_addr += size * 3;
    } else if (!strcmp(token, ASMDIR_BYTE)) {
        token = nextToken();
        //Suppose to have literal(s) after byte
        if(!ckptr(token)){
            printf("[ERROR] Syntax error\n");
            return -1;
        }
        //call parseLiterals
        uint32_t literal_len = 0;
        uint32_t *literal = parseLiteral(token, &literal_len);
        if (!ckptr(literal)) return -1;
        //append literal no matter what, even it's data are duplicated
        appendLiteral(literal, *curr_section, line, *curr_addr, literal_len, 0);
        *curr_addr += literal_len;
    } else if (!strcmp(token, ASMDIR_WORD)) {
        //parse plain integer
        token = nextToken();
        if (!ckptr(token)) return -1;
        uint32_t *literal = malloc(sizeof(uint32_t));
        if (!ckptr(literal)) return - 1;
        *literal = atoi(token);
        appendLiteral(literal, *curr_section, line, *curr_addr, 3, 0);
        *curr_addr += 3;
    } else if (!strcmp(token, ASMDIR_USE)) {
        return parseUse(nextToken(), curr_addr, curr_section);
    } else if (!strcmp(token, ASMDIR_END)) {
        // return to root scope
        parseUse(NULL, curr_addr, curr_section);
        return parseLTORG(curr_section, curr_addr);
    } else if (!strcmp(token, ASMDIR_LTORG)) {
        return parseLTORG(curr_section, curr_addr);
    } else if (!strcmp(token, ASMDIR_EXTREF)) {
        //add all refs to symtab
        do {
            token = nextToken();
            if (!ckptr(token)) break;
            trimStr(token);
            size_t i = strlen(token) - 1;
            if (token[i] == ',') {
                token[i] = '\0';
            }
            //if symbol already existed
            if (symbolExist(token)) {
                Symbol *sym = lookupSymbol(token);
                if (!ckptr(sym)) return -1;
                if (sym->flag & SYMFLAG_EXTDEF) return -1;
                sym->flag = SYMFLAG_EXTREF;
            } else {
                // if not, append it
                appendSymbol(token, 0, line, *curr_section, SYMFLAG_EXTREF);
            }
        } while (ckptr(token));
    } else if (!strcmp(token, ASMDIR_EXTDEF)) {
        //add all refs to symtab
        do {
            token = nextToken();
            if (!ckptr(token)) break;
            trimStr(token);
            size_t i = strlen(token) - 1;
            if (token[i] == ',') {
                token[i] = '\0';
            }
            //if symbol already existed
            if (symbolExist(token)) {
                Symbol *sym = lookupSymbol(token);
                if (!ckptr(sym)) return -1;
                if (sym->flag & SYMFLAG_EXTREF) return -1;
                sym->flag = SYMFLAG_EXTDEF;
            } else {
                // if not, append it
                appendSymbol(token, 0, line, *curr_section, SYMFLAG_EXTDEF);
            }
        } while (ckptr(token));
    } else if (!strcmp(token, ASMDIR_EQU)) {
        Symbol *curr, *tmp;
        HASH_ITER(hh, symtab_head, curr, tmp){
            if(curr->line == line){
                curr->flag |= SYMFLAG_EQUDEF;
                break;
            }
        }
        if (!curr) return -1;
        //must have next token
        token = nextToken();
        if (!ckptr(token)) return -1;
        // printf("expr: %s\n", token);
        //Indicate current address
        if(token[0] == '*'){
            curr->address = *curr_addr;
            return 0;
        }

        char *temp = malloc(strlen(token));
        if (!ckptr(temp)) return -1;
        strcpy(temp, token);
        char *var_1 = strtok(temp, "+-*/");
        uint32_t lhs = 0;
        if (isdigit(var_1[0])) {
            lhs = atoi(var_1);
        } else {
            Symbol *sym = lookupSymbol(var_1);
            if (!ckptr(sym)){
                free(temp);
                return -1;
            }
            lhs = sym->address;
        }
        char *var_2 = nextToken();
        if (!ckptr(var_2)) {
            // only var_1
            curr->address = lhs;
            free(temp);
            return 0;
        }
        trimStr(var_2);
        uint32_t rhs = 0;
        if (isdigit(var_2[0])) {
            rhs = atoi(var_2);
        } else {
            Symbol *sym = lookupSymbol(var_2);
            if (!ckptr(sym)){
                free(temp);
                return -1;
            }
            rhs = sym->address;
        }

        char op = token[strlen(var_1)];
        switch (op) {
            case '+':
                curr->address = lhs + rhs;
                break;
            case '-':
                curr->address = lhs - rhs;
                break;
            case '*':
                curr->address = lhs * rhs;
                break;
            case '/':
                curr->address = lhs / rhs;
                break;

            default:
                free(temp);
                return -1;
                break;
        }
        free(temp);
    } else if (!strcmp(token, ASMDIR_BASE)) {
    }

    return 0;
}

int parseUse(char *token, uint32_t *curr_addr, uint32_t *curr_section) {
    // writeback section
    Block *blk = getBlock(*curr_section);
    if (!ckptr(blk)) {
        printf("[ERROR] Cannot writeback\n");
        return -1;
    }
    blk->length = *curr_addr;
    // if only USE
    if (!ckptr(token)) {
        // change to root block
        Block *blk = getBlock(0);
        *curr_section = 0;
        *curr_addr = blk->length;
        return 0; // goto next line
    }
    // if it is USE block_name
    trimStr(token);
    //if block not exist
    if (!blockExist(token)) {
        Block *blk = appendBlock(token, 0, 0);
        if (!ckptr(blk)) {
            printf("[ERROR] Cannot append block\n");
            return -1;
        }
        *curr_section = blk->section_id;
        *curr_addr = blk->length;
    } else {
        // block already exist
        Block *blk = lookupBlock(token);
        *curr_addr = blk->length;
        *curr_section = blk->section_id;
    }
    return 0;
}

//dump all literal
int parseLTORG(uint32_t *curr_sid, uint32_t *curr_addr) {
    Literal *curr, *tmp;
    Block *section;
    section = getBlock(*curr_sid);
    if (!ckptr(section)) return -1;
    HASH_ITER(hh, literaltab_head, curr, tmp) {
        //if relocate flag is set
        if(curr->relocate){
            curr->section_id = *curr_sid;
            curr->address = *curr_addr;
            *curr_addr += curr->length;
            curr->relocate = 0; // set the flag, so END won't process it
            section->length += curr->length;
            printf("DUMP line: %d | data: %06x | addr: %06x\n", curr->line, curr->data, curr->address);
        }
    }
    return 0;
}

uint32_t* parseLiteral(char *token, uint32_t *len_out) {
    //sanity check
    if (strlen(token) < 4) return NULL;
    //capture string inside '
    //also skip the '
    int literal_len = 0;
    for (size_t i = 2; i < strlen(token); i++)
    {
        if(token[i] == '\0'){
            printf("[ERROR] Syntax error");
            return NULL;
        }
        if (token[i] == '\'') break;
        literal_len++;
    }

    // after finding the range of the literal
    // we put it into somewhere
    char *literal_str = malloc(literal_len);
    memcpy(literal_str, &token[2], literal_len);
    if (token[0] == 'C') {
        //As character
        if(literal_len > 3){
            printf("[ERROR] More than 3 characters\n");
            free(literal_str);
            return NULL;
        }
        //endian problem
        if(literal_len == 2){
            literal_str = ((*(uint16_t *)literal_str) & 0xff) << 8 | ((*(uint16_t *)literal_str) & 0xff00) >> 8;
        }else if(literal_len == 3){
            char temp = literal_str[0];
            literal_str[0] = literal_str[2];
            literal_str[2] = temp;
        }
        // strrev(literal_str);
        if(ckptr(len_out)){
            *len_out = literal_len;
        }
        return literal_str; // do nothing
    } else if (token[0] == 'X') {
        //As heximal
        
        //Make sure all the characters are valid
        for (auto i = 0; i < strlen(literal_str); i++){
            if(!isxdigit(literal_str[i])){
                printf("[ERROR] Literal contains non heximal character: %c\n", literal_str[i]);
                free(literal_str);
                return NULL;
            }
        }
        // it must not exceed 24bit a.k.a 3 bytes
        if (literal_len > 6 || literal_len % 2 != 0) {
            printf("[ERROR] Not 24bit heximal\n");
            free(literal_str);
            return NULL;
        }
        uint32_t *literal_int = malloc(sizeof(uint32_t));
        *literal_int = strtol(literal_str, NULL, 16);
        free(literal_str);
        if(ckptr(len_out)){
            *len_out = literal_len / 2;
        }
        return literal_int;
    }
    return NULL;
}
