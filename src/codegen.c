#include "sicxeasm.h"
#include "uthash/utlist.h"

int32_t genObjcode(FILE *fp, InstructionNode *head, ProgramHdr *prog_hdr) {
    if (writeHeaderRecord(fp, prog_hdr) == -1) return -1;
    InstructionNode *curr, *tmp;
    uint8_t text_len = 0; //in bytes
    InstructionNode *start = NULL;
    LL_FOREACH(head, curr) {
        uint8_t inst_len = 0;
        if (!ckptr(start)) {
            start = curr;
            continue;
        }
        if(!ckptr(curr->next)){
            writeTextRecord(fp, start, curr->next);
            continue;
        }
        if(curr->break_here){
            writeTextRecord(fp, start, curr);
            text_len = 0;
            start = curr;
            continue;
        }

        switch (curr->next->type) {
            case Inst_1:
                inst_len = 1;
                break;

            case Inst_2:
                inst_len = 2;
                break;

            case Inst_3:
                inst_len = 3;
                break;

            case Inst_4:
                inst_len = 4;
                break;

            default:
                break;
        }
        
        if(text_len + inst_len >= 30){
            writeTextRecord(fp, start, curr);
            text_len = 0;
            start = curr;
            continue;
        }
        
        text_len += inst_len;
    }
    //dump all literals
    char *buff = malloc(60);
    if (!ckptr(buff)) return -1;
    Literal *literal, *lit_tmp, *prev;
    text_len = 0;
    HASH_SORT(literaltab_head, sort_by_address);
    HASH_ITER(hh, literaltab_head, literal, lit_tmp) {
        if (!ckptr(prev)) {
            prev = literal;
            continue;
        }
        if(ckptr(literal)){
            fprintf(fp, "T%06X%02X%02X\n", literal->address, literal->length, literal->data);
            continue;
        }
    }
    //write end record
    fprintf(fp, "E%06X", prog_hdr->start_addr);
    return 0;
}

int sort_by_address(Literal *a, Literal *b){
    return a->address - b->address;
}

// write records to files
int32_t writeHeaderRecord(FILE *fp, ProgramHdr *hdr) {
    if (!valid24bit(hdr->start_addr)) return -1;
    if (!valid24bit(hdr->prog_length)) return -1;
    return fprintf(fp, "H%-6s%06X%06X\n", hdr->prog_name, hdr->start_addr, hdr->prog_length);
}

int32_t writeTextRecord(FILE *fp, InstructionNode *start, InstructionNode *end) {
    InstructionNode *curr = start;
    uint8_t length = 0;
    while (curr != end) {
        switch(curr->type){
            case Inst_1:
                length += 1;
                break;
            case Inst_2:
                length += 2;
                break;
            case Inst_3:
                length += 3;
                break;
            case Inst_4:
                length += 4;
                break;
        }
        curr = curr->next;
    }

    fprintf(fp, "T%06X%02X", start->address, length);
    curr = start; //reset
    while(curr != end){
        switch(curr->type){
            case Inst_1:
                fprintf(fp, "%02X", *(curr->inst_ptr.inst_1));
                break;
            case Inst_2:
                fprintf(fp, "%04X", *(curr->inst_ptr.inst_2));
                break;
            case Inst_3:
                fprintf(fp, "%06X", *(curr->inst_ptr.inst_3));
                break;
            case Inst_4:
                fprintf(fp, "%08X", *(curr->inst_ptr.inst_4));
                break;
        }
        curr = curr->next;
    }
    fprintf(fp, "\n");
    return 0;
}