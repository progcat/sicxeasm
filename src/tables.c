#include "sicxeasm.h"
#include "uthash/uthash.h"

extern Symbol *symtab_head = NULL;
extern Block *blktab_head = NULL;
extern Literal *literaltab_head = NULL;

void resolveBlkAddr(){
    Block *curr, *tmp, *prev = NULL;
    HASH_ITER(hh, blktab_head, curr, tmp) {
        if(!ckptr(prev)){
            prev = curr;
            continue;
        }
        curr->address = prev->address + prev->length;
        prev = curr;
    }
}

void resolveAddresses(){
    Block *curr, *tmp = NULL;
    HASH_ITER(hh, blktab_head, curr, tmp) {
        uint32_t sid = curr->section_id;
        Symbol *sym, *sym_tmp;
        //find symbol that in this address and its in this section
        HASH_ITER(hh, symtab_head, sym, sym_tmp){
            //skip external symbols & EQU defines
            if (sym->flag & SYMFLAG_EXTREF || sym->flag & SYMFLAG_EQUDEF) continue;
            if (sym->section == sid) {
                sym->address += curr->address;
            }
        }

        Literal *lit, *lit_tmp;
        HASH_ITER(hh, literaltab_head, lit, lit_tmp){
            if (lit->section_id == sid){
                lit->address += curr->address;
            }
        }
    }
}

int appendSymbol(char *ident, uint32_t addr, uint32_t line, uint32_t sid, uint8_t flag){
    Symbol *item = malloc(sizeof(Symbol));
    if (!ckptr(item)) return -1;
    item->ident = malloc(strlen(ident));
    if (!ckptr(item)) return -1;
    strcpy(item->ident, ident);
    item->address = addr;
    item->section = sid;
    item->line = line;
    item->flag = flag;

    HASH_ADD_STR(symtab_head, ident, item);
    return 0;
}

Symbol* lookupSymbol(char *ident){
    Symbol *res;
    HASH_FIND_STR(symtab_head, ident, res);
    return res;
}

int symbolExist(char *ident){
    Symbol* res = lookupSymbol(ident);
    if(ckptr(res)){
        return 1;
    }
    return 0;
}

void printSymtab(){
    Symbol *curr, *tmp;
    printf("| line | addr/blk | ident | flag |\n");
    printf("----------------------------------\n");
    HASH_ITER(hh, symtab_head, curr, tmp) {
        printf("| %4d | %04x %d | %5s | %c%c%c |\n", \
        curr->line, curr->address, curr->section, curr->ident, curr->flag & SYMFLAG_EXTDEF ? 'E' : 'L', curr->flag & SYMFLAG_EXTREF ? 'E' : 'L', curr->flag & SYMFLAG_EQUDEF ? 'E' : 'S');
    }
}

void freeAllSymbols(){
    Symbol *curr, *tmp;
    HASH_ITER(hh, symtab_head, curr, tmp) {
        HASH_DEL(symtab_head, curr);
        free(curr);
    }
}


Block* appendBlock(char *ident, uint32_t addr, uint32_t length){
    static uint32_t sid = 0;
    Block *item = malloc(sizeof(Block));
    if (!ckptr(item)) return NULL;
    item->ident = malloc(strlen(ident));
    if (!ckptr(item)) return -1;
    strcpy(item->ident, ident);
    item->address = addr;
    item->section_id = sid;
    item->length = length;

    HASH_ADD_INT(blktab_head, section_id, item);
    sid++;
    return item;
}

Block* lookupBlock(char *ident){
    Block *res, *tmp;
    HASH_ITER(hh, blktab_head, res, tmp){
        if (!strcmp(ident, res->ident)) return res;
    }
    return NULL;
}

Block* getBlock(uint32_t sid){
    Block *res;
    HASH_FIND_INT(blktab_head, &sid, res);
    return res;
}

int blockExist(char *ident){
    Block* res = lookupBlock(ident);
    if(ckptr(res)){
        return 1;
    }
    return 0;
}

void freeAllBlocks(){
    Block *curr, *tmp;
    HASH_ITER(hh, blktab_head, curr, tmp) {
        HASH_DEL(blktab_head, curr);
        free(curr);
    }
}

void printBlktab(){
    Block *curr, *tmp;
    printf("| sid | addr | ident | len |\n");
    printf("----------------------------------\n");
    HASH_ITER(hh, blktab_head, curr, tmp) {
        printf("| %3d | %04x | %5s | %04x |\n",\
        curr->section_id, curr->address, curr->ident, curr->length);
    }
}

int appendLiteral(uint32_t *data, uint32_t sid, uint32_t line, uint32_t address, uint32_t length, uint8_t need_relocate){
    Literal *literal = malloc(sizeof(Literal));
    if (!ckptr(literal)) return -1;
    literal->section_id = sid;
    literal->line = line;
    literal->data = *data;
    literal->address = address;
    literal->length = length;
    literal->relocate = need_relocate;
    HASH_ADD_INT(literaltab_head, data, literal);
    return 0;
}

Literal* lookupLiteral(uint32_t data){
    Literal *res;
    HASH_FIND_INT(literaltab_head, &data, res);
    return res;
}

int existLiteral(uint32_t data){
    Literal *res = lookupLiteral(data);
    if(ckptr(res)){
        return 1;
    }
    return 0;
}

void freeAllLiterals(){
    Literal *curr, *tmp = NULL;
    HASH_ITER(hh, literaltab_head, curr, tmp) {
        HASH_DEL(literaltab_head, curr);
        free(curr);
    }
}

void printLiteraltab(){
    Literal *curr, *tmp;
    printf("| line | sid | addr | len | content | relocate |\n");
    printf("----------------------------------\n");
    HASH_ITER(hh, literaltab_head, curr, tmp) {
        printf("| %4d | %2d  | %04X | %3d | %08X | %c |\n",
               curr->line, curr->section_id, curr->address, curr->length, curr->data, curr->relocate ? 'T' : 'F');
    }
}