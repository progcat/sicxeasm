#include "sicxeasm.h"
#include "opcode.h"
#include "uthash/utlist.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

InstructionNode *inst_head = NULL;

char *nextToken() {
    return strtok(NULL, " ");
}

int32_t compile(FILE *in_fp, FILE *out_fp, size_t fsize) {
    //
    //  Build program header
    //
    char *buff = malloc(fsize);
    if (!ckptr(buff)) {
        printf("could not allocate buffer");
	return -1;
    }

    ProgramHdr *prog_hdr = malloc(sizeof(ProgramHdr));
    if (!ckptr(prog_hdr)) {
	printf("could not allocate prog_hdr");
        free(buff);
        return -1;
    }

    //Read first line for program header
    if (!ckptr(fgets(buff, fsize, in_fp))) {
	printf("could not read first line");
        free(buff);
        return -1;
    }
    //read program header
    char *token = strtok(buff, " ");
    if (!ckptr(token)) {
	    printf("could not tokenize program header");
        free(buff);
        return -1;
    }

    // The first token is program name
    if (strlen(token) > 6) return -1;
    prog_hdr->prog_name = malloc(6);
    if (!ckptr(prog_hdr->prog_name)) return -1;
    strcpy(prog_hdr->prog_name, token);
    // the second one is START, skip
    token = nextToken();
    // if nothing here
    if (!ckptr(token)) {
        free(buff);
        return -1;
    }
    // the third is Address
    token = nextToken();
    // fetch address as hex
    prog_hdr->start_addr = strtol(token, NULL, 16);
    free(buff);
    printf("Start first pass\n");
    //
    // First Pass: Build symbol table
    //
    if (buildTables(in_fp, fsize, prog_hdr->start_addr)) return -1;
    // resolve block
    resolveBlkAddr();
    //after calculating block's address, we now know the length og the program
    Block *last_blk = getBlock(HASH_COUNT(blktab_head) - 1);
    prog_hdr->prog_length = last_blk->address + last_blk->length;

    //DEBUG: print symbol table
#ifdef DEBUG
    printf("\n[DEBUG] Tables after resolveBlkAddr\n");
    printf("---Symbols---\n");
    printSymtab();
    printf("\n---Blocks---\n");
    printBlktab();
    printf("\n---Literals---\n");
    printLiteraltab();
#endif
    //resolve symbols and literal address
    resolveAddresses();
#ifdef DEBUG
    printf("\n[DEBUG] Tables after resolveAddresses\n");
    printf("---Symbols---\n");
    printSymtab();
    printf("\n---Blocks---\n");
    printBlktab();
    printf("\n---Literals---\n");
    printLiteraltab();
#endif
    //
    // Second Pass: Code Generation
    //
    printf("Start second pass\n");
    printf("Translate to machine code\n");
    if (parseOpCode(in_fp, fsize, prog_hdr->start_addr)) return -1;
    printf("Write to output file\n");
    if (genObjcode(out_fp, inst_head, prog_hdr) == -1) return -1;

    //retract in_fp
    fseek(in_fp, 0, SEEK_SET);

    return 0;
}

//initialize tables, fetch all literals from lines, calculate addresses
int32_t buildTables(FILE *fp, size_t fsize, uint32_t start_loc) {
    char *buff = malloc(fsize);
    if (!ckptr(buff)) {
        return -1;
    }
    char *token = strtok(buff, " ");
    size_t line = 1;
    uint32_t curr_section = 0;
    uint32_t curr_addr = 0;
    // create root block
    if (!ckptr(appendBlock("\0", start_loc, 0))) {
        printf("[ERROR] Cannot append root block!\n");
        free(buff);
        return -1;
    }

    while (ckptr(fgets(buff, fsize, fp))) {
        
        // Comment
        if (buff[0] == '.' || buff[0] == '\n') {
            // skip this line, line counter++
            line++;
            continue;
        }
        line++;

        token = strtok(buff, " \n");
        if (!ckptr(token)) continue;

#ifdef DEBUG
        printf("[DEBUG] line: %d | token: %s (%d) | curr_addr: %06x\n", line, token, strlen(token), curr_addr);
#endif
        trimStr(token);
        //if it is label
        if (!isOpCode(token) && token[0] != '+' && !isDirective(token)) {
            
            // append if not existed
            if (symbolExist(token)) {
                printf("Symbol %s already existed!");
                free(buff);
                return -1;
            }
            appendSymbol(token, curr_addr, line, curr_section, SYMFLAG_LOCAL);
            // countine to proceed address calculation
            token = nextToken();
            trimStr(token);
        }

        //if it is a directive
        if (isDirective(token)) {
            if (parseDirective(token, &curr_addr, &curr_section, line)) {
                free(buff);
                return -1;
            }
            continue;
        }

        //if it is op
        if (isOpCode(token) || token[0] == '+') {
            // instruction start with '+' is type 4 but only for Fmt34 Opcode
            int type4 = token[0] == '+' ? 1 : 0;
            //skip the plus sign if needed
            token = type4 ? token + 1 : token;
            int res = lookupOpcode(token);
            if (res == -1) return -1;
            enum OpType type = OPCODE[res].fmt;
            // type1 and type2 with '+' is not allowed
            if (type != Fmt34 && type4 == 1) {
                free(buff);
                return -1;
            }

            switch (type) {
                case Fmt1:
                    curr_addr += sizeof(InstType1);
                    break;
                case Fmt2:
                    curr_addr += sizeof(InstType2);
                    break;
                case Fmt34:
                    if (type4) {
                        curr_addr += sizeof(InstType4);
                    } else {
                        curr_addr += sizeof(InstType3);
                    }
                    token = nextToken();
                    // if we found a literal
                    if(ckptr(token) && token[0] == '='){
                        token++;
                        uint32_t literal_len = 0;
                        uint32_t *literal;
                        if (token[0] == '*') {
                            // * - current address
                            literal = malloc(sizeof(uint32_t));
                            if (!ckptr(literal)) return -1;
                            *literal = curr_addr;
                        } else {
                            literal = parseLiteral(token, &literal_len);
                            if (!ckptr(literal)) return -1;
                        }
                        //will not append if already existed
                        if (!existLiteral(*literal)) {
                            appendLiteral(literal, curr_section, line, curr_addr, literal_len, 1);
                        }
                    }
                    break;
            }
        }
    }
    free(buff);
    return 0;
}

//parse opcode, create instructions
int32_t parseOpCode(FILE *fp, size_t fsize, uint32_t start_loc){
    uint32_t base = 0;
    char *buff = malloc(fsize);
    if (!ckptr(buff)) {
        printf("could not allocate memory\n");
        return -1;
    }

    //rewind to the second line
    fseek(fp, 1, SEEK_SET);

    char *token = strtok(buff, " ");
    size_t line = 0;
    uint32_t curr_section = 0;
    uint32_t curr_addr = 0;
    uint8_t prev_break = 0;
    while (ckptr(fgets(buff, fsize, fp))) {
        // Comment
        if (buff[0] == '.' || buff[0] == '\n') {
            // skip this line, line counter++
            line++;
            continue;
        }
        line++;
        token = strtok(buff, " \n");
        if (!ckptr(token)) continue;
        trimStr(token);
        //skip labels if needed
        if (!isOpCode(token) && token[0] != '+'){
            token = nextToken();
            if (!ckptr(token) || !isOpCode(token)) {prev_break = 1; continue;}
        }
        //now we sure thats a opcode
#ifdef DEBUG
        printf("[DEBUG] line: %d | OP: %s (%d) | curr_addr: %06x\n", line, token, strlen(token), curr_addr);
#endif
        uint8_t is_type_4 = token[0] == '+' ? 1 : 0;
        if (is_type_4) token++; //skip '+'
        int32_t op_idx = lookupOpcode(token);
        //in case of nothing
        if (op_idx < 0) {
            printf("[ERROR] Opcode %s not found!\n", token);
            return -1;
        }
        //allocate memory for InstructionNode
        InstructionNode *inst = malloc(sizeof(InstructionNode));
        if (!ckptr(inst)) {
            printf("[ERROR] Could not allocate memory for InstructionNode!\n");
            return -1;
        }
        inst->break_here = prev_break;
        prev_break = 0;
        uint8_t op = OPCODE[op_idx].mcode;
        if (is_type_4) {
            // Type 1 and Type 2 with '+' sign is not allowed
            if (OPCODE[op_idx].fmt != Fmt34) {
                printf("[ERROR] Opcode %s is not allowed to be type 4!\n", token);
                return -1;
            }
            //It is Type 4
            inst->inst_ptr.inst_4 = malloc(sizeof(InstType4));
            if (!ckptr(inst->inst_ptr.inst_4)) {
                printf("[ERROR] Could not allocate memory for InstructionType4!\n");
                return -1;
            }
            inst->type = Inst_4;
            inst->inst_ptr.inst_4->op = op;
            inst->inst_ptr.inst_4->e = 1;
        } else {
            if (OPCODE[op_idx].fmt == Fmt34){
                //It is Type 3
                inst->inst_ptr.inst_3 = malloc(sizeof(InstType3));
                if (!ckptr(inst->inst_ptr.inst_3)) {
                    printf("[ERROR] Could not allocate memory for InstructionType3!\n");
                    return -1;
                }

                inst->type = Inst_3;
                inst->inst_ptr.inst_3->op = op;
                inst->inst_ptr.inst_3->e = 0;
            } else {
                // Type 1 and Type 2
                inst->type = OPCODE[op_idx].fmt;
                if(inst->type == Inst_1){
                    //It is Type 1
                    inst->inst_ptr.inst_1 = malloc(sizeof(InstType1));
                    if (!ckptr(inst->inst_ptr.inst_1)) {
                        printf("[ERROR] Could not allocate memory for InstructionType1!\n");
                        return -1;
                    }
                    inst->inst_ptr.inst_1 = op;
                }else{
                    //It is Type 2
                    inst->inst_ptr.inst_2 = malloc(sizeof(InstType2));
                    if (!ckptr(inst->inst_ptr.inst_2)) {
                        printf("[ERROR] Could not allocate memory for InstructionType2!\n");
                        return -1;
                    }
                    inst->inst_ptr.inst_2->op = op;
                }
            }
        }

        uint8_t operand_n = OPCODE[op_idx].operand_n;
        int8_t r1, r2;
        uint8_t n;
        
        //fetch operands
        if (operand_n == 1) {
            token = nextToken();
            if (token[0] == '+') token++;
            char *operand_a;
            char *operand_b;
            switch (OPCODE[op_idx].operand_a) {
                case REG:
                    //it must be Type 2
                    if (parseReg(token, &r1) == -1){
                        printf("[ERROR] Invalid register %s!\n", token);
                        return -1;
                    }
                    inst->inst_ptr.inst_2->r1 = r1;
                    inst->inst_ptr.inst_2->r2 = 0;
                    break;
                case ADDR:
                    //it can be Type 3 or Type 4
                    //check if operand is a literal, if it is a literal, return literal address
                    operand_a = strtok(token, " ,");
                    trimStr(operand_a);
                    operand_b = nextToken();
                    if (ckptr(operand_b)) trimStr(operand_b);
                    uint32_t address;
                    printf("[DEBUG] operand_a: %s | operand_b: %s\n", operand_a, operand_b);
                    if (parseAddr(operand_a, &address) == -1) {
                        printf("[ERROR] Invalid address %s!\n", operand_a);
                        return -1;
                    }

                    if (is_type_4) {
                        inst->inst_ptr.inst_4->address = address;
                        if(token[0] == '#'){
                            inst->inst_ptr.inst_4->op |= 1;
                        }else if(token[0] == '@'){
                            inst->inst_ptr.inst_4->op |= 2;
                        }else{
                            inst->inst_ptr.inst_4->op |= 3;
                        }
                        inst->inst_ptr.inst_4->x = ckptr(operand_b) ? 1 : 0;
                        inst->inst_ptr.inst_4->b = 0;
                        inst->inst_ptr.inst_4->p = 0;
                    } else {
                        // inst->inst_ptr.inst_3->address = curr_addr;
                        uint32_t pc = curr_addr + 3; //pc is always point to the next command
                        int32_t displacement = address - pc;
                        if(displacement >= -2048 && displacement <= 2047){
                            //pc related
                            inst->inst_ptr.inst_3->b = 0;
                            inst->inst_ptr.inst_3->p = 1;
                            inst->inst_ptr.inst_3->disp = displacement;
                        }else if (displacement >= 0 && displacement <= 4095) {
                            //base related
                            inst->inst_ptr.inst_3->b = 1;
                            inst->inst_ptr.inst_3->p = 0;
                            inst->inst_ptr.inst_3->disp = (address - base);
                        }else{
                            inst->inst_ptr.inst_3->b = 0;
                            inst->inst_ptr.inst_3->p = 0;
                            inst->inst_ptr.inst_3->disp = address;
                        }
                        if(token[0] == '#'){
                            inst->inst_ptr.inst_3->disp = address;
                            inst->inst_ptr.inst_3->op |= 1;
                            inst->inst_ptr.inst_3->b = 0;
                            inst->inst_ptr.inst_3->p = 0;
                        }else if(token[0] == '@'){
                            inst->inst_ptr.inst_3->op |= 2;
                        }else{
                            inst->inst_ptr.inst_3->op |= 3;
                        }
                        inst->inst_ptr.inst_3->x = ckptr(operand_b) ? 1 : 0;
                    }
                    break;
                case LITERAL:
                    //it must be Type 2
                    if (parseN(token, &n) == -1) {
                        printf("[ERROR] Invalid literal %s!\n", token);
                        return -1;
                    }
                    inst->inst_ptr.inst_2->r1 = n;
                    inst->inst_ptr.inst_2->r2 = 0;
                    break;
                default:
                    break;
            }
        }
        else if (operand_n == 2) {
            token = nextToken();
            if (!ckptr(token)) {
                printf("[ERROR] Invalid operand!\n");
                return -1;
            }
            trimStr(token);
            token = strtok(token, " ,");
            if (!ckptr(token)) {
                printf("[ERROR] Invalid operand!\n");
                return -1;
            }
            char *operand_a = token;
            if (!ckptr(operand_a)) {
                printf("[ERROR] Invalid operand!\n");
                return -1;
            }
            char *operand_b = nextToken();
            if (!ckptr(operand_b)) {
                printf("[ERROR] Invalid operand!\n");
                return -1;
            }

            // Operand A
            if (OPCODE[op_idx].operand_a != REG) {
                printf("[ERROR] Invalid operand!\n");
                return -1;
            }
            // it must be REG
            if (parseReg(operand_a, &r1) == -1) {
                printf("[ERROR] Invalid register %s!\n", operand_a);
                return -1;
            }
            inst->inst_ptr.inst_2->r1 = r1;
            
            //Operand B
            switch (OPCODE[op_idx].operand_b)
            {
            case REG:
                if (parseReg(operand_b, &r2) == -1) {
                    printf("[ERROR] Invalid register %s!\n", operand_b);
                    return -1;
                }
                inst->inst_ptr.inst_2->r2 = r2;
                break;
            case LITERAL:
                if(parseN(operand_b, &n) == -1) {
                    printf("[ERROR] Invalid literal %s!\n", operand_b);
                    return -1;
                }
                inst->inst_ptr.inst_2->r2 = n;
                break;
            default:
                // you shouldn't be here
                return -1;
                break;
            }
        }
        //special case, RSUB
        if(inst->type == Inst_3 && inst->inst_ptr.inst_3->op == 0x4c){
            inst->inst_ptr.inst_3->op = 0x4f;
        }
        //append instruction to the list
        inst->address = curr_addr;
        LL_APPEND(inst_head, inst);

        switch (OPCODE[op_idx].fmt) {
            case Fmt1:
                curr_addr += sizeof(InstType1);
                break;
            case Fmt2:
                curr_addr += sizeof(InstType2);
                break;
            case Fmt34:
                if (is_type_4) {
                    curr_addr += sizeof(InstType4);
                } else {
                    curr_addr += sizeof(InstType3);
                }
        }
    }

    free(buff);
    return 0;
}

int32_t isDirective(char *str) {
    if (!ckptr(str)) return 0;
    if (!strcmp(str, ASMDIR_START)) return 1;
    if (!strcmp(str, ASMDIR_END)) return 1;
    if (!strcmp(str, ASMDIR_BYTE)) return 1;
    if (!strcmp(str, ASMDIR_WORD)) return 1;
    if (!strcmp(str, ASMDIR_RESB)) return 1;
    if (!strcmp(str, ASMDIR_RESW)) return 1;
    if (!strcmp(str, ASMDIR_USE)) return 1;
    if (!strcmp(str, ASMDIR_LTORG)) return 1;
    if (!strcmp(str, ASMDIR_EXTDEF)) return 1;
    if (!strcmp(str, ASMDIR_EXTREF)) return 1;
    if (!strcmp(str, ASMDIR_EQU)) return 1;
    return 0;
}

int32_t lookupOpcode(const char *str) {
    for (auto i = 0; i < OPCODE_LEN; i++) {
        if (!strcmp(str, OPCODE[i].token)) return i;
    }
    return -1;
}

/*
    return 1 if it is reserved word
    return 0 if it is not
*/
int32_t isOpCode(const char *token) {
    if (!ckptr(token)) return 0;
    for (auto i = 0; i < OPCODE_LEN; i++) {
        if (!strcmp(token, OPCODE[i].token)) return 1;
    }
    return 0;
}

void trimStr(char *str){
    if (isspace(*str)) return;
    char *ptr = str + strlen(str) - 1;
    while (isspace(*ptr) && str < ptr)
        ptr--;
    ptr[1] = '\0';
}

int32_t parseReg(char *token, uint8_t *reg_out){
    if (!ckptr(token)) return -1;
    trimStr(token);
    if (token[0] == ',') token++;
    switch (token[0]) {
        case 'A':
            *reg_out = REG_A;
            break;
        case 'X':
            *reg_out = REG_X;
            break;
        case 'L':
            *reg_out = REG_L;
            break;
        case 'B':
            *reg_out = REG_B;
            break;
        case 'S':
            *reg_out = REG_S;
            break;
        case 'T':
            *reg_out = REG_T;
            break;
        case 'F':
            *reg_out = REG_F;
            break;
        default:
            return -1;
            break;
    }
    return 0;
}

int32_t parseAddr(char *token, uint32_t *addr_out){
    if (!ckptr(token)) {
            printf("[ERROR] Invalid token!\n");
            return -1;
        }
    switch (token[0])
    {
    case '#': //立即定址
        //can be decimal or symbol
        token++;
        if (isdigit(token[0])) {
            *addr_out = atoi(token);
        } else {
            Symbol *sym = lookupSymbol(token);
            if (!ckptr(sym)){
                printf("[ERROR] Symbol %s not found!\n", token);
                return -1;
            }
            *addr_out = sym->address;
        }
        return 0;
        break;

    case '@': //間接定址
        //must be symbol
        token++;
        Symbol *sym = lookupSymbol(token);
        if (!ckptr(sym)){
            printf("[ERROR] Symbol %s not found!\n", token);
            return -1;
        }
        *addr_out = sym->address;
        return 0;
        break;

    case '=': //取常數地址
        //must in literal table
        token++;
        uint32_t *data = parseLiteral(token, NULL);
        Literal *literal = lookupLiteral(*data);
        if (!ckptr(literal)) {
            printf("[ERROR] Literal %s not found!\n", token);
            return -1;
        }
        *addr_out = literal->address;
        return 0;
        break;
    default:
        //maybe it is a Symbol
        if (symbolExist(token)) {
            Symbol *sym = lookupSymbol(token);
            *addr_out = sym->address;
            return 0;
        }
        break;
    }
    return -1;
}

int32_t parseN(char *token, uint8_t *n_out){
    printf("parseN: %s\n", token);
    return 0;
}
