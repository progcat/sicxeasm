================
    SICASM
作者： ProgCat
================

需求：
gcc/clang
make

使用方法:

* 編譯SICASM："make"
* 清除生成檔："make clean"
* 執行SICASM："sicasm [源代碼路徑]"

預設的編譯器是clang。
如要用其他編譯器，可以在Makefile中修改"CC"

Libraries Used:
* uthash (https://github.com/troydhanson/uthash)
